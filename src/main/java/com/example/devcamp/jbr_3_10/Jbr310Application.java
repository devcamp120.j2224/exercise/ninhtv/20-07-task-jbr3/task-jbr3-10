package com.example.devcamp.jbr_3_10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr310Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr310Application.class, args);
	}

}
